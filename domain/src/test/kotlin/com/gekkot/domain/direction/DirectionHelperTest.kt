package com.gekkot.domain.direction

import org.junit.Assert
import org.junit.Test


internal class DirectionHelperTest {

    companion object {
        private const val MAX_DELTA: Double = 0.01
    }

    @Test
    fun degreeToRad() {
        val degree: Double = 180.0
        val expectedRad:Double = Math.PI
        val actualRad = DirectionHelper.degreeToRad(degree = degree)
        Assert.assertEquals(expectedRad, actualRad, MAX_DELTA)


    }

    @Test
    fun radToDegree() {
        val rad = Math.PI
        val expectedDegree = 180.0
        val actualDegree = DirectionHelper.radToDegree(rad = rad)
        Assert.assertEquals(expectedDegree,actualDegree, MAX_DELTA)
    }


}