package com.gekkot.domain.speed

import com.gekkot.domain.location.ILocation
import com.gekkot.domain.location.Location
import org.junit.Test

import org.junit.Assert.*

class DistanceHelperTest {

    @Test
    fun distanceLocation() {
        val lat1: Double = 58.397791
        val long1: Double = 19.174953

        val lat2: Double = 58.342007
        val long2: Double = 19.294809

        val expectedDistance = 9440.0

        val location1 : ILocation = Location(long1, lat1)
        val location2 : ILocation = Location(long2, lat2)
        val actualDistance = DistanceHelper.distance(location1, location2)
        assertEquals(expectedDistance,actualDistance, MAX_DELTA)
    }


    @Test
    fun distanceBetweenSameLocation(){
        val lat1: Double = 58.397791
        val long1: Double = 19.174953

        val exceptedDistance = 0.0
        val actualDistance = DistanceHelper.distance(lat1,long1,lat1,long1)
        assertEquals(exceptedDistance, actualDistance, ZERO_DELTA)

    }
    @Test
    fun distanceLatLong() {
        val lat1: Double = 58.397791
        val long1: Double = 19.174953

        val lat2: Double = 58.342007
        val long2: Double = 19.294809

        val expectedDistance = 9440.0
        val actualDistance = DistanceHelper.distance(lat1, long1, lat2, long2)
        println("distance = $actualDistance")


        assertEquals(expectedDistance, actualDistance, MAX_DELTA)
    }

    companion object {
        private const val MAX_DELTA = 300.0;
        private const val ZERO_DELTA = 0.0;
    }
}