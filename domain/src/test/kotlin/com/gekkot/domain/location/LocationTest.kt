package com.gekkot.domain.location

import org.junit.Assert
import org.junit.Test


internal class LocationTest {

    @Test
    fun getLatitude() {
        val latitudeValue = 2.0
        val location:ILocation = Location(longitude = 1.0,latitude = latitudeValue)
        Assert.assertEquals(latitudeValue, location.getLatitude(),0.0)
    }

    @Test
    fun getLongitude() {
        val longitudeValue = 4.4
        val location: ILocation = Location(longitude = longitudeValue, latitude =  1.0)
        Assert.assertEquals(longitudeValue, location.getLongitude(), 0.0)

    }
}