package com.gekkot.domain.trackpoint

import com.gekkot.domain.direction.Direction
import com.gekkot.domain.direction.DirectionHelper
import com.gekkot.domain.location.ILocation
import com.gekkot.domain.location.Location
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.*

internal class DirectionTrackPointTest {

    @Test
    fun constructorTest(){
        val radians: Double = DirectionHelper.degreeToRad(180.0)
        val direction = Direction(radians)
        val timestamp = Calendar.getInstance().time.time
        val location : ILocation = Location(45.0, 46.0)
        val directionTrackPoint = DirectionTrackPoint(direction,timestamp,location)

        assertEquals(direction, directionTrackPoint.direction)
        assertEquals(location, directionTrackPoint.location)
        assertEquals(timestamp, directionTrackPoint.timestamp)
    }
}