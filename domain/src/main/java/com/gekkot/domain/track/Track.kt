package com.gekkot.domain.track

import com.gekkot.domain.trackpoint.TrackPoint
import java.util.*

class Track {

    private var trackPoints: MutableList<TrackPoint> = LinkedList()

    fun addTrackPoint(trackPoint: TrackPoint) {
        trackPoints.add(trackPoint)
    }
}