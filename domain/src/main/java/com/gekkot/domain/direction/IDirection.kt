package com.gekkot.domain.direction

interface IDirection {
    fun getDirection():Double
}