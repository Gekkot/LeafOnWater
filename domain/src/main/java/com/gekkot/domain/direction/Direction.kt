package com.gekkot.domain.direction

class Direction constructor(private val direction: Double) : IDirection {

    override fun getDirection(): Double {
        return direction
    }
}