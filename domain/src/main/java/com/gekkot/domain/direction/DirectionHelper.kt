package com.gekkot.domain.direction

object DirectionHelper {
    private const val DEGREE_TO_RAD_CONST = Math.PI / 180
    private const val RAD_TO_DEGREE_CONST = 1 / DEGREE_TO_RAD_CONST;
    fun degreeToRad(degree: Double): Double {
        return degree * DEGREE_TO_RAD_CONST
    }

    fun radToDegree(rad: Double): Double {
        return RAD_TO_DEGREE_CONST* rad;
    }
}