package com.gekkot.domain.speed

import com.gekkot.domain.location.ILocation

object DistanceHelper {
    private const val EARTH_RADIUS_METRES = 6371 * 1000


    fun distance(location1: ILocation, location2 : ILocation): Double{
        return distance(
            location1.getLatitude(),
            location1.getLongitude(),
            location2.getLatitude(),
            location2.getLongitude())
    }

    fun distance(
        lat1: Double,
        long1: Double,
        lat2: Double,
        long2: Double
    ): Double {
        val latDistance = Math.toRadians(lat2 - lat1)
        val longDistance = Math.toRadians(long2 - long1)
        val halfLatDistance = latDistance /2
        val halfLongDistance = longDistance /2
        val a = (Math.sin(halfLatDistance) * Math.sin(halfLatDistance)
                + (Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(halfLongDistance) * Math.sin(halfLongDistance)))
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        return EARTH_RADIUS_METRES * c
    }
}