package com.gekkot.domain.location

class Location(private val longitude: Double, private val latitude: Double) : ILocation {
    override fun getLatitude(): Double {
        return this.latitude
    }

    override fun getLongitude(): Double {
        return this.longitude
    }


}