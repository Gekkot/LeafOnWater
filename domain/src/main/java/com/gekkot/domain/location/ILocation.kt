package com.gekkot.domain.location

interface ILocation {
    fun getLatitude(): Double
    fun getLongitude(): Double
}