package com.gekkot.domain.trackpoint

import com.gekkot.domain.direction.IDirection
import com.gekkot.domain.location.ILocation

class DirectionTrackPoint(var direction: IDirection, timestamp: Long, location: ILocation) :
    TrackPoint(timestamp, location) {
}